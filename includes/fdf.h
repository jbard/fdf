/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/12 17:31:37 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 14:32:50 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FDF_H
# define __FDF_H

# include "mlx.h"
# include "get_next_line.h"
# include "color.h"
# include "key.h"

# define SMALL_LOGO_PATH "./assets/logo_FDF_216x151"
# define MEDIUM_LOGO_PATH "./assets/logo_FDF_432x301"
# define LARGE_LOGO_PATH "./assets/logo_FDF_864x602"

# define BOOT_LINE_PATH "./assets/boot_lines"

# define BOOT_START 1
# define BOOT_STOP 0

# define BOOT_STEP_1 2
# define BOOT_STEP_2 1
# define BOOT_STEP_3 0
# define BOOT_STEP_4 3
# define BOOT_STEP_5 4
# define BOOT_STEP_6 5

# define FLIP_ON 4
# define FLIP_OFF 0

# define Y 1
# define N 0

# define WIN_W 2560
# define WIN_H 1440

# define IMG(y) mlx->img.y
# define LINE(y) mlx->img.line.y
# define MAP(y) mlx->img.map.y
# define BOOT(y) mlx->img.boot.y
# define MENU(y) mlx->img.menu.y

typedef struct	s_line
{
	int		x1;
	int		x2;
	int		y1;
	int		y2;
	int		color;
}				t_line;

typedef struct	s_map
{
	int		***coor;
	int		map_width;
	int		map_height;
	int		coor_max;
	int		coor_min;

	int		map;
	int		flip;
	int		neg;
	int		color1;
	int		color2;
	int		color3;
	int		diag1;
	int		diag2;
	int		alt;

	double	size;
	double	place_x;
	double	place_y;
}				t_map;

typedef struct	s_boot
{
	int		step;
	int		boot;
	int		fd;
	int		boot_line_y;
	int		draw_hud;
}				t_boot;

typedef struct	s_menu
{
	int		menu;
	int		param;

	int		color1;
	int		color2;
	int		color3;
}				t_menu;

typedef	struct	s_img
{
	void	*img_ptr;
	int		*data;
	int		size_l;
	int		bpp;
	int		endian;

	t_line	line;
	t_map	map;
	t_boot	boot;
	t_menu	menu;
}				t_img;

typedef struct	s_mlx
{
	void	*mlx_ptr;
	void	*win;
	t_img	img;
}				t_mlx;

int				ft_create_tab(t_mlx *mlx, char *file);
void			ft_create_all_coor(t_mlx *mlx);
void			ft_draw_line(t_mlx *mlx);
void			ft_init_events(t_mlx *mlx);
int				ft_draw_logo(t_mlx *mlx, char *path, int x, int y);
int				ft_boot_loop(t_mlx *mlx);
void			ft_boot_key(int key, t_mlx *mlx);
int				ft_draw_loading(t_mlx *mlx);
void			ft_draw_all(t_mlx *mlx);
int				ft_draw_hud(t_mlx *mlx);
void			ft_draw_menu(t_mlx *mlx, int key);
void			ft_coor_to_line(int x1, int y1, int x2, int y2);
void			ft_draw_map(t_mlx *mlx, int map);
void			ft_init_var(t_mlx *mlx);
void			ft_draw_color_menu(t_mlx *mlx, int key);
void			ft_draw_properties_menu(t_mlx *mlx, int key);
int				ft_color(int z1, int z2, t_mlx *mlx);
void			ft_center_map(t_mlx *mlx);
int				ft_draw_destructured_hud(t_mlx *mlx, int step);
void			ft_case_check(t_mlx *mlx, int x, int y, int check);
void			ft_draw_arrow(t_mlx *mlx, int space, int on);
void			ft_malloc_error(t_mlx *mlx);
void			ft_clean_menu(t_mlx *mlx);
int				ft_init_color_light(int color);
int				ft_init_color_vibrant(int color);
int				ft_init_color_dark(int color);
void			ft_exit_properly(t_mlx *mlx);
int				ft_init_color_var(t_mlx *mlx, int c);
void			ft_init_boot_var(t_mlx *mlx);
void			ft_set_x_y(t_mlx *mlx, int x, int y);
t_mlx			*ft_get_mlx(t_mlx *mlx);
void			ft_draw_rotation_menu_enter(t_mlx *mlx);
void			ft_draw_color_menu_str_4(t_mlx *mlx);

#endif
