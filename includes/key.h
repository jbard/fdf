/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 21:05:03 by jbard             #+#    #+#             */
/*   Updated: 2018/02/13 21:05:09 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __KEY_H
# define __KEY_H

# define KEY_ENTER 36
# define KEY_ESCAPE 53
# define KEY_SUPR 51
# define KEY_UP 126
# define KEY_DOWN 125
# define KEY_LEFT 123
# define KEY_RIGHT 124

#endif
