/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 21:03:03 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 12:23:52 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __COLOR_H
# define __COLOR_H

# define LIGHT_GREEN 0x67bff4b
# define VIBRANT_GREEN 0x55ff19
# define DARK_GREEN 0x188500

# define LIGHT_BLUE 0x4b93ff
# define VIBRANT_BLUE 0x1974ff
# define DARK_BLUE 0x002a85

# define LIGHT_PURPLE 0xce4bff
# define VIBRANT_PURPLE 0xc219ff
# define DARK_PURPLE 0x6d0085

# define LIGHT_PINK 0xff4bbc
# define VIBRANT_PINK 0xff19a9
# define DARK_PINK 0x850048

# define LIGHT_RED 0xff514b
# define VIBRANT_RED 0xff2219
# define DARK_RED 0x851000

# define LIGHT_ORANGE 0xff754b
# define VIBRANT_ORANGE 0xff5019
# define DARK_ORANGE 0x852b00

# define LIGHT_YELLOW 0xf2ff4b
# define VIBRANT_YELLOW 0xedff19
# define DARK_YELLOW 0x708500

# define LIGHT_WHITE 0xededed
# define VIBRANT_WHITE 0xf4f4f4
# define DARK_WHITE 0x4f4f4f

/*
** COLOR
** You can choose another colors by modifying defines.
**
** LIGHT_COLOR, VIBRANT_COLOR, DARK_COLOR and are GLOBAL COLORS.
**
** You can also choose to modify independently each SECONDARY COLOR by
** replacing COLOR between parentheses in the define.
**
** Avaible colors :
** -> GREEN | BLUE | PURPLE | PINK | RED | ORANGE | YELLOW | WHITE.
*/

/*
** GLOBAL COLORS
*/

# define LIGHT_COLOR LIGHT_GREEN
# define VIBRANT_COLOR VIBRANT_GREEN
# define DARK_COLOR DARK_GREEN

/*
** SECONDARY COLORS
*/

# define MAP_LIGHT_COLOR LIGHT_COLOR
# define MAP_VIBRANT_COLOR VIBRANT_COLOR
# define MAP_DARK_COLOR DARK_COLOR

# define HUD_LIGHT_COLOR LIGHT_COLOR
# define HUD_VIBRANT_COLOR VIBRANT_COLOR
# define HUD_DARK_COLOR DARK_COLOR

# define LOGO_LIGHT_COLOR LIGHT_COLOR
# define LOGO_VIBRANT_COLOR VIBRANT_COLOR
# define LOGO_DARK_COLOR DARK_COLOR

#endif
