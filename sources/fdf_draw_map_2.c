/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_draw_map_2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 18:45:27 by jbard             #+#    #+#             */
/*   Updated: 2018/02/13 20:09:49 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		ft_color(int z1, int z2, t_mlx *mlx)
{
	int		diff;

	diff = MAP(coor_max) - MAP(coor_min);
	if (z1 <= MAP(coor_min) + diff / 4
			&& z2 <= MAP(coor_min) + diff / 4)
		return (MAP(color1));
	else if (z1 >= MAP(coor_max) - diff / 4
			&& z2 >= MAP(coor_max) - diff / 4)
		return (MAP(color3));
	else
		return (MAP(color2));
}

void	ft_center_map(t_mlx *mlx)
{
	MAP(size) = 0;
	MAP(place_x) = 0;
	MAP(place_y) = 0;
	while ((MAP(map_width) + MAP(map_height)) / 2 *
			MAP(size) < WIN_H - WIN_H / 4)
		MAP(size) += 0.1;
	while ((MAP(map) % 2 == 0 && (MAP(place_x) + MAP(map_width) -
					MAP(place_y)) * MAP(size) < WIN_W - WIN_W / 80)
			|| (MAP(map) % 2 && (MAP(place_x) + MAP(map_height) -
					MAP(place_y)) * MAP(size) < WIN_W - WIN_W / 80))
	{
		MAP(place_x) += 0.2;
		MAP(place_y) -= 0.15;
	}
}
