/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_draw_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/24 18:26:39 by jbard             #+#    #+#             */
/*   Updated: 2018/02/13 18:31:52 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_coor_to_line(int x1, int y1, int x2, int y2)
{
	t_mlx	*mlx;

	mlx = NULL;
	mlx = ft_get_mlx(mlx);
	LINE(x1) = x1;
	LINE(y1) = y1;
	LINE(x2) = x2;
	LINE(y2) = y2;
	ft_draw_line(mlx);
}

static void	ft_swap_coor(t_line *line)
{
	int	tmp;

	if (line->x1 < line->x2)
	{
		tmp = line->x1;
		line->x1 = line->x2;
		line->x2 = tmp;
		tmp = line->y1;
		line->y1 = line->y2;
		line->y2 = tmp;
	}
}

static void	ft_draw_pixel(t_mlx *mlx)
{
	if (BOOT(draw_hud) || (LINE(x1) >= (WIN_W / 9 + 1)
				&& LINE(x1) <= (WIN_W - 2)
				&& LINE(y1) >= 2 && LINE(y1) <= WIN_H - 47))
		IMG(data)[LINE(y1) * WIN_W + LINE(x1)] = LINE(color);
}

void		ft_draw_line(t_mlx *mlx)
{
	int ex;
	int ey;
	int err;
	int e;

	ft_swap_coor(&IMG(line));
	ex = abs(LINE(x2) - LINE(x1));
	ey = abs(LINE(y2) - LINE(y1));
	err = (ex > ey ? ex : -ey) / 2;
	while (LINE(x1) != LINE(x2) || LINE(y1) != LINE(y2))
	{
		e = err;
		ft_draw_pixel(mlx);
		if (e > -ex)
		{
			LINE(x1)--;
			err -= ey;
		}
		if (e < ey)
		{
			LINE(y1) > LINE(y2) ? LINE(y1)-- : LINE(y1)++;
			err += ex;
		}
	}
	ft_draw_pixel(mlx);
}
