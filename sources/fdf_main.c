/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_main.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/16 10:45:08 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 14:43:36 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_check_extension(char *file)
{
	int		i;

	i = 0;
	while (file[i])
	{
		i++;
		if (file[i] == '.' && !ft_strcmp(file + i, ".fdf"))
			return (0);
	}
	ft_putendl("error: bad extension");
	return (1);
}

static void	ft_free_all(t_mlx *mlx)
{
	int		i;
	int		j;

	if (mlx)
	{
		i = -1;
		if (MAP(coor))
		{
			while (++i < 8 && MAP(coor)[i])
			{
				j = -1;
				while (MAP(coor)[i][j] && ((i % 2 == 0 && ++j < MAP(map_height))
						|| (i % 2 == 1 && ++j < MAP(map_width))))
					free(MAP(coor)[i][j]);
				free(MAP(coor)[i]);
			}
			if (mlx->mlx_ptr)
			{
				mlx_destroy_window(mlx->mlx_ptr, mlx->win);
				mlx_destroy_image(mlx->mlx_ptr, IMG(img_ptr));
			}
			free(MAP(coor));
		}
		free(mlx);
	}
}

void		ft_exit_properly(t_mlx *mlx)
{
	ft_free_all(mlx);
	exit(0);
}

void		ft_malloc_error(t_mlx *mlx)
{
	ft_putendl("error: memory allocation fail");
	ft_exit_properly(mlx);
}

int			main(int argc, char **argv)
{
	t_mlx	*mlx;

	if (argc == 2)
	{
		if (!(mlx = ft_memalloc(sizeof(t_mlx))))
			ft_malloc_error(mlx);
		if (!(MAP(coor) = (int ***)ft_memalloc(sizeof(int **) * 8)))
			ft_malloc_error(mlx);
		if (!(MAP(coor)[0] = (int **)ft_memalloc(sizeof(int *))))
			ft_malloc_error(mlx);
		MAP(map_height) = 0;
		MAP(map_width) = 0;
		if (ft_check_extension(argv[1]) || ft_create_tab(mlx, argv[1]))
			return (1);
		ft_create_all_coor(mlx);
		mlx->mlx_ptr = mlx_init();
		mlx->win = mlx_new_window(mlx->mlx_ptr, WIN_W, WIN_H, "FDF");
		IMG(img_ptr) = mlx_new_image(mlx->mlx_ptr, WIN_W, WIN_H);
		IMG(data) = (int *)mlx_get_data_addr(IMG(img_ptr),
				&IMG(bpp), &IMG(size_l), &IMG(endian));
		ft_init_events(mlx);
	}
	else
		ft_putendl("usage: ./fdf file_name.fdf");
	return (0);
}
