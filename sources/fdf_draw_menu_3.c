/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_draw_menu_3.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 20:49:34 by jbard             #+#    #+#             */
/*   Updated: 2018/02/16 11:41:33 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_draw_properties_menu_param(t_mlx *mlx)
{
	if (MENU(param) % 6 == 5)
	{
		MENU(menu) = 0;
		MENU(param) = 0;
	}
	else if (MENU(param) % 6 == 4)
		ft_init_var(mlx);
	else if (MENU(param) % 6 == 0 && MAP(neg == -1))
		MAP(neg) = 1;
	else if (MENU(param) % 6 == 0)
		MAP(neg) = -1;
	else if (MENU(param) % 6 == 1 && !MAP(diag1))
		MAP(diag1) = Y;
	else if (MENU(param) % 6 == 1)
		MAP(diag1) = N;
	else if (MENU(param) % 6 == 2 && !MAP(diag2))
		MAP(diag2) = Y;
	else if (MENU(param) % 6 == 2)
		MAP(diag2) = N;
}

static void	ft_draw_properties_menu_str_2(t_mlx *mlx)
{
	mlx_string_put(mlx->mlx_ptr, mlx->win, 93, 290,
			HUD_VIBRANT_COLOR, "PROPERTIES");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 65, 470,
			HUD_DARK_COLOR, "REVERSE ALTITUDE");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 95, 520,
			HUD_DARK_COLOR, "DIAGONAL 1");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 95, 570,
			HUD_DARK_COLOR, "DIAGONAL 2");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 100, 620,
			HUD_DARK_COLOR, "ALTITUDE");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 115, 670, HUD_DARK_COLOR, "RESET");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 720, HUD_DARK_COLOR, "RETURN");
}

static void	ft_draw_properties_menu_str_1(t_mlx *mlx)
{
	if (MENU(param) % 6 == 0)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 65, 470,
				HUD_VIBRANT_COLOR, "REVERSE ALTITUDE");
	else if (MENU(param) % 6 == 1)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 95, 520,
				HUD_VIBRANT_COLOR, "DIAGONAL 1");
	else if (MENU(param) % 6 == 2)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 95, 570,
				HUD_VIBRANT_COLOR, "DIAGONAL 2");
	else if (MENU(param) % 6 == 3)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 100, 620,
				HUD_VIBRANT_COLOR, "ALTITUDE");
	else if (MENU(param) % 6 == 4)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 115, 670,
				HUD_VIBRANT_COLOR, "RESET");
	else
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 720,
				HUD_VIBRANT_COLOR, "RETURN");
}

void		ft_draw_properties_menu(t_mlx *mlx, int key)
{
	ft_case_check(mlx, 135, 490, MAP(neg) - 1);
	ft_case_check(mlx, 135, 540, MAP(diag1));
	ft_case_check(mlx, 135, 590, MAP(diag2));
	ft_set_x_y(mlx, 120, 646);
	ft_draw_arrow(mlx, 30, 3);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
	if (key == KEY_ENTER)
		ft_draw_properties_menu_param(mlx);
	else if (key == KEY_DOWN && MENU(param) < 5)
		MENU(param)++;
	else if (key == KEY_DOWN)
		MENU(param) = 0;
	else if (key == KEY_UP && MENU(param) > 0)
		MENU(param)--;
	else if (key == KEY_UP)
		MENU(param) = 5;
	else if (key == KEY_RIGHT && MENU(param) % 5 == 3)
		MAP(alt) += 1;
	else if (key == KEY_LEFT && MENU(param) % 5 == 3)
		MAP(alt) -= 1;
	ft_draw_properties_menu_str_2(mlx);
	ft_draw_properties_menu_str_1(mlx);
}
