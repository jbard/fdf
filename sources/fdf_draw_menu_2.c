/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_draw_menu_2.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 20:32:07 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 14:30:22 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_draw_color_set(t_mlx *mlx, int key)
{
	if (key == KEY_RIGHT && MENU(param) % 4 == 0 && MENU(color1) < 7)
		MENU(color1)++;
	else if (key == KEY_LEFT && MENU(param) % 4 == 0 && MENU(color1) > 0)
		MENU(color1)--;
	else if (key == KEY_RIGHT && MENU(param) % 4 == 1 && MENU(color2) < 7)
		MENU(color2)++;
	else if (key == KEY_LEFT && MENU(param) % 4 == 1 && MENU(color2) > 0)
		MENU(color2)--;
	else if (key == KEY_RIGHT && MENU(param) % 4 == 2 && MENU(color3) < 7)
		MENU(color3)++;
	else if (key == KEY_LEFT && MENU(param) % 4 == 2 && MENU(color3) > 0)
		MENU(color3)--;
	MAP(color1) = ft_init_color_light(MENU(color1));
	MAP(color2) = ft_init_color_vibrant(MENU(color2));
	MAP(color3) = ft_init_color_dark(MENU(color3));
}

static void	ft_draw_color_menu_str_1(t_mlx *mlx)
{
	mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 290,
			HUD_VIBRANT_COLOR, "COLOR");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 85, 520,
			HUD_DARK_COLOR, "LIGHT COLOR");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 80, 570,
			HUD_DARK_COLOR, "VIBRANT COLOR");
	if (MAP(color1) == LIGHT_GREEN)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 115, 535, LIGHT_GREEN, "GREEN");
	else if (MAP(color1) == LIGHT_BLUE)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 535, LIGHT_BLUE, "BLUE");
	else if (MAP(color1) == LIGHT_PURPLE)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 535,
				LIGHT_PURPLE, "PURPLE");
	else if (MAP(color1) == LIGHT_PINK)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 535, LIGHT_PINK, "PINK");
	else if (MAP(color1) == LIGHT_RED)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 125, 535, LIGHT_RED, "RED");
	else if (MAP(color1) == LIGHT_ORANGE)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 535,
				LIGHT_ORANGE, "ORANGE");
	else if (MAP(color1) == LIGHT_YELLOW)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 535,
				LIGHT_YELLOW, "YELLOW");
	else
		mlx_string_put(mlx->mlx_ptr, mlx->win, 115, 535, LIGHT_WHITE, "WHITE");
}

static void	ft_draw_color_menu_str_2(t_mlx *mlx)
{
	mlx_string_put(mlx->mlx_ptr, mlx->win, 90, 620,
			HUD_DARK_COLOR, "DARK COLOR");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 670,
			HUD_DARK_COLOR, "RETURN");
	if (MAP(color2) == VIBRANT_GREEN)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 115, 585,
				VIBRANT_GREEN, "GREEN");
	else if (MAP(color2) == VIBRANT_BLUE)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 585, VIBRANT_BLUE, "BLUE");
	else if (MAP(color2) == VIBRANT_PURPLE)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 585,
				VIBRANT_PURPLE, "PURPLE");
	else if (MAP(color2) == VIBRANT_PINK)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 585, VIBRANT_PINK, "PINK");
	else if (MAP(color2) == VIBRANT_RED)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 125, 585, VIBRANT_RED, "RED");
	else if (MAP(color2) == VIBRANT_ORANGE)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 585,
				VIBRANT_ORANGE, "ORANGE");
	else if (MAP(color2) == VIBRANT_YELLOW)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 585,
				VIBRANT_YELLOW, "YELLOW");
	else
		mlx_string_put(mlx->mlx_ptr, mlx->win, 115, 585,
				VIBRANT_WHITE, "WHITE");
}

static void	ft_draw_color_menu_str_3(t_mlx *mlx)
{
	ft_draw_color_menu_str_4(mlx);
	if (MAP(color3) == DARK_GREEN)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 115, 635, DARK_GREEN, "GREEN");
	else if (MAP(color3) == DARK_BLUE)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 635, DARK_BLUE, "BLUE");
	else if (MAP(color3) == DARK_PURPLE)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 635, DARK_PURPLE, "PURPLE");
	else if (MAP(color3) == DARK_PINK)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 635, DARK_PINK, "PINK");
	else if (MAP(color3) == DARK_RED)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 125, 635, DARK_RED, "RED");
	else if (MAP(color3) == DARK_ORANGE)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 635, DARK_ORANGE, "ORANGE");
	else if (MAP(color3) == DARK_YELLOW)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 635, DARK_YELLOW, "YELLOW");
	else
		mlx_string_put(mlx->mlx_ptr, mlx->win, 115, 635, DARK_WHITE, "WHITE");
}

void		ft_draw_color_menu(t_mlx *mlx, int key)
{
	if (key == KEY_ENTER && MENU(param) == 3)
	{
		MENU(menu) = 0;
		MENU(param) = 0;
	}
	ft_set_x_y(mlx, 103, 546);
	ft_draw_arrow(mlx, 62, 0);
	ft_set_x_y(mlx, 103, 596);
	ft_draw_arrow(mlx, 62, 1);
	ft_set_x_y(mlx, 103, 646);
	ft_draw_arrow(mlx, 62, 2);
	if (key == KEY_DOWN && MENU(param) < 3)
		MENU(param)++;
	else if (key == KEY_DOWN)
		MENU(param) = 0;
	else if (key == KEY_UP && MENU(param) > 0)
		MENU(param)--;
	else if (key == KEY_UP)
		MENU(param) = 3;
	if (key == KEY_RIGHT || key == KEY_LEFT)
		ft_draw_color_set(mlx, key);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
	ft_draw_color_menu_str_1(mlx);
	ft_draw_color_menu_str_2(mlx);
	ft_draw_color_menu_str_3(mlx);
}
