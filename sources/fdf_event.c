/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_event.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 20:58:36 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 14:41:05 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_key(int key, t_mlx *mlx)
{
	if (key == KEY_ESCAPE)
		ft_exit_properly(mlx);
	else if (BOOT(boot))
		ft_boot_key(key, mlx);
	else
		ft_draw_menu(mlx, key);
	if (!BOOT(boot) && (key == KEY_LEFT || key == KEY_RIGHT
				|| key == KEY_ENTER))
		ft_draw_all(mlx);
	else if (!BOOT(boot))
	{
		mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
		ft_draw_menu(mlx, 0);
	}
	return (0);
}

void		ft_draw_all(t_mlx *mlx)
{
	ft_putendl("1");
	mlx_clear_window(mlx->mlx_ptr, mlx->win);
	mlx_destroy_image(mlx->mlx_ptr, IMG(img_ptr));
	IMG(img_ptr) = mlx_new_image(mlx->mlx_ptr, WIN_W, WIN_H);
	IMG(data) = (int *)mlx_get_data_addr(IMG(img_ptr),
			&IMG(bpp), &IMG(size_l), &IMG(endian));
	ft_draw_hud(mlx);
	ft_draw_map(mlx, MAP(map) + MAP(flip));
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
	ft_draw_menu(mlx, 0);
}

void		ft_init_events(t_mlx *mlx)
{
	ft_init_boot_var(mlx);
	mlx_key_hook(mlx->win, ft_key, mlx);
	mlx_loop_hook(mlx->mlx_ptr, ft_boot_loop, mlx);
	mlx_loop(mlx);
}
