/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_draw_menu.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 18:46:34 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 15:45:21 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_draw_rotation_menu_str(t_mlx *mlx)
{
	mlx_string_put(mlx->mlx_ptr, mlx->win, 100, 290,
			HUD_VIBRANT_COLOR, "ROTATION");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 550, HUD_DARK_COLOR, "ROTATE");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 600, HUD_DARK_COLOR, "FLIP");
	if (MENU(param) % 3 == 0)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 550,
				HUD_VIBRANT_COLOR, "ROTATE");
	else if (MENU(param) % 3 == 1)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 600,
				HUD_VIBRANT_COLOR, "FLIP");
	else
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 670,
				HUD_VIBRANT_COLOR, "RETURN");
	if (MAP(map) == 0)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 136, 565,
				HUD_VIBRANT_COLOR, "0");
	else if (MAP(map) == 1)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 131, 565,
				HUD_VIBRANT_COLOR, "90");
	else if (MAP(map) == 2)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 126, 565,
				HUD_VIBRANT_COLOR, "180");
	else
		mlx_string_put(mlx->mlx_ptr, mlx->win, 126, 565,
				HUD_VIBRANT_COLOR, "270");
}

static void	ft_draw_rotation_menu(t_mlx *mlx, int key)
{
	if (key == KEY_ENTER)
		ft_draw_rotation_menu_enter(mlx);
	else if (key == KEY_DOWN && MENU(param) < 2)
		MENU(param)++;
	else if (key == KEY_DOWN)
		MENU(param) = 0;
	else if (key == KEY_UP && MENU(param) > 0)
		MENU(param)--;
	else if (key == KEY_UP)
		MENU(param) = 2;
	ft_case_check(mlx, 135, 620, MAP(flip));
	ft_set_x_y(mlx, 120, 576);
	ft_draw_arrow(mlx, 30, 0);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
	if (key == KEY_RIGHT && MENU(param) % 3 == 0 && MAP(map) < 3)
		MAP(map)++;
	else if (key == KEY_LEFT && MENU(param) % 3 == 0 && MAP(map) > 0)
		MAP(map)--;
	mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 670, HUD_DARK_COLOR, "RETURN");
	ft_draw_rotation_menu_str(mlx);
}

static void	ft_draw_main_menu_str(t_mlx *mlx, int menu_tmp)
{
	mlx_string_put(mlx->mlx_ptr, mlx->win, 100, 230,
			HUD_DARK_COLOR, "ROTATION");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 117, 270, HUD_DARK_COLOR, "COLOR");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 93, 310,
			HUD_DARK_COLOR, "PROPERTIES");
	mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 370, HUD_DARK_COLOR, "EXIT");
	if (menu_tmp % 5 == 1)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 100, 230,
				HUD_VIBRANT_COLOR, "ROTATION");
	else if (menu_tmp % 5 == 2)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 117, 270,
				HUD_VIBRANT_COLOR, "COLOR");
	else if (menu_tmp % 5 == 3)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 93, 310,
				HUD_VIBRANT_COLOR, "PROPERTIES");
	else if (menu_tmp % 5 == 4)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 120, 370,
				HUD_VIBRANT_COLOR, "EXIT");
}

static void	ft_draw_main_menu(t_mlx *mlx, int key)
{
	static int	menu_tmp;

	if (!menu_tmp)
		menu_tmp = 1;
	if (key == KEY_ENTER)
	{
		MENU(menu) = menu_tmp % 5;
		ft_draw_menu(mlx, 0);
	}
	if (key == KEY_DOWN && menu_tmp < 4)
		menu_tmp++;
	else if (key == KEY_DOWN)
		menu_tmp = 1;
	else if (key == KEY_UP && menu_tmp > 1)
		menu_tmp--;
	else if (key == KEY_UP)
		menu_tmp = 4;
	ft_draw_main_menu_str(mlx, menu_tmp);
}

void		ft_draw_menu(t_mlx *mlx, int key)
{
	void (*f[4])(t_mlx*, int);

	if (MENU(menu) % 4 == 0 && MENU(menu) != 0)
		ft_exit_properly(mlx);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
	f[0] = &(ft_draw_main_menu);
	f[1] = &(ft_draw_rotation_menu);
	f[2] = &(ft_draw_color_menu);
	f[3] = &(ft_draw_properties_menu);
	f[MENU(menu)](mlx, key);
}
