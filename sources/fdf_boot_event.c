/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_boot_event.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 17:28:57 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 12:25:34 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_str(t_mlx *mlx)
{
	t_line	line;
	char	*str;

	if (BOOT(step) == 2)
		BOOT(fd) = open(BOOT_LINE_PATH, O_RDONLY);
	if ((BOOT(step) = get_next_line(BOOT(fd), &str)))
	{
		line.y1 = 1 * BOOT(boot_line_y);
		line.color = HUD_VIBRANT_COLOR;
		mlx_string_put(mlx->mlx_ptr, mlx->win, 1, line.y1, line.color, str);
		free(str);
		BOOT(boot_line_y) += 20;
		usleep(400000);
	}
	else
	{
		close(BOOT(fd));
		free(str);
	}
	return (0);
}

static void	ft_boot_loop_2(t_mlx *mlx)
{
	if (BOOT(step) == BOOT_STEP_5)
	{
		if (!ft_draw_loading(mlx))
			BOOT(step) = BOOT_STEP_6;
		mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
	}
	else if (BOOT(step) == BOOT_STEP_6)
	{
		BOOT(boot) = BOOT_STOP;
		ft_draw_all(mlx);
	}
}

int			ft_boot_loop(t_mlx *mlx)
{
	if (!BOOT(boot))
		return (0);
	else if (BOOT(step) == BOOT_STEP_1 || BOOT(step) == BOOT_STEP_2)
		ft_str(mlx);
	else if (BOOT(step) == BOOT_STEP_4)
	{
		usleep(2000000);
		mlx_clear_window(mlx->mlx_ptr, mlx->win);
		mlx_destroy_image(mlx->mlx_ptr, IMG(img_ptr));
		IMG(img_ptr) = mlx_new_image(mlx->mlx_ptr, WIN_W, WIN_H);
		IMG(data) = (int *)mlx_get_data_addr(IMG(img_ptr),
				&IMG(bpp), &IMG(size_l), &IMG(endian));
		BOOT(step) = BOOT_STEP_5;
	}
	else
		ft_boot_loop_2(mlx);
	return (0);
}

void		ft_boot_key(int key, t_mlx *mlx)
{
	if (key == KEY_SUPR)
	{
		BOOT(boot) = BOOT_STOP;
		ft_draw_all(mlx);
	}
	else if (BOOT(step) == BOOT_STEP_3 && key == KEY_ENTER)
	{
		ft_draw_logo(mlx, LARGE_LOGO_PATH, WIN_W / 2 - 432, WIN_H / 2 - 400);
		mlx_put_image_to_window(mlx->mlx_ptr, mlx->win, IMG(img_ptr), 0, 0);
		mlx_string_put(mlx->mlx_ptr, mlx->win,
				1600, 900, HUD_VIBRANT_COLOR, "par JBARD");
		BOOT(step) = BOOT_STEP_4;
	}
}
