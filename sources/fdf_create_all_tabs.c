/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_create_all_tabs.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 17:39:58 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 15:47:08 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_tab_flip_coor(t_mlx *mlx, int tab)
{
	int		i;
	int		j;
	int		k;

	if (!(MAP(coor)[tab + 4] = (int **)ft_memalloc(sizeof(int *) *
					MAP(map_height))))
		ft_malloc_error(mlx);
	i = 0;
	while (i < MAP(map_height))
	{
		j = 0;
		k = MAP(map_width) - 1;
		if (!(MAP(coor)[tab + 4][i] =
					(int *)ft_memalloc(sizeof(int) * MAP(map_width))))
			ft_malloc_error(mlx);
		while (j < MAP(map_width))
		{
			MAP(coor)[tab + 4][i][k] = MAP(coor)[tab][i][j];
			j++;
			k--;
		}
		i++;
	}
}

static void	ft_inclined_tab_flip_coor(t_mlx *mlx, int tab)
{
	int		i;
	int		j;
	int		k;

	if (!(MAP(coor)[tab + 4] = (int **)ft_memalloc(sizeof(int *) *
					MAP(map_width))))
		ft_malloc_error(mlx);
	i = 0;
	while (i < MAP(map_width))
	{
		j = 0;
		k = MAP(map_height) - 1;
		if (!(MAP(coor)[tab + 4][i] =
					(int *)ft_memalloc(sizeof(int) * MAP(map_height))))
			ft_malloc_error(mlx);
		while (j < MAP(map_height))
		{
			MAP(coor)[tab + 4][i][k] = MAP(coor)[tab][i][j];
			j++;
			k--;
		}
		i++;
	}
}

static void	ft_reverse_tab_coor(t_mlx *mlx, int tab)
{
	int		i;
	int		j;
	int		k;
	int		l;

	if (!(MAP(coor)[tab + 2] = (int **)ft_memalloc(sizeof(int *) *
					MAP(map_height))))
		ft_malloc_error(mlx);
	i = -1;
	l = MAP(map_height) - 1;
	while (++i < MAP(map_height))
	{
		j = -1;
		k = MAP(map_width) - 1;
		if (!(MAP(coor)[tab + 2][l] = ft_memalloc(sizeof(int) *
						MAP(map_width))))
			ft_malloc_error(mlx);
		while (++j < MAP(map_width))
		{
			MAP(coor)[tab + 2][l][k] = MAP(coor)[tab][i][j];
			k--;
		}
		l--;
	}
	ft_tab_flip_coor(mlx, tab + 2);
}

static void	ft_incline_tab_coor(t_mlx *mlx, int tab)
{
	int		i;
	int		j;
	int		k;

	if (!(MAP(coor)[tab + 1] = (int **)ft_memalloc(sizeof(int *) *
					MAP(map_width))))
		ft_malloc_error(mlx);
	i = -1;
	j = -1;
	while (++j < MAP(map_width))
	{
		if (!(MAP(coor)[tab + 1][j] =
					(int *)ft_memalloc(sizeof(int) * MAP(map_height))))
			ft_malloc_error(mlx);
	}
	k = MAP(map_height) - 1;
	while (++i < MAP(map_height))
	{
		j = -1;
		while (++j < MAP(map_width))
			MAP(coor)[tab + 1][j][k] = MAP(coor)[tab][i][j];
		k--;
	}
	ft_inclined_tab_flip_coor(mlx, tab + 1);
}

void		ft_create_all_coor(t_mlx *mlx)
{
	ft_tab_flip_coor(mlx, 0);
	ft_incline_tab_coor(mlx, 0);
	ft_reverse_tab_coor(mlx, 0);
	ft_incline_tab_coor(mlx, 2);
}
