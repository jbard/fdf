/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_draw_img.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 17:56:29 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 14:49:25 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			ft_draw_hud(t_mlx *mlx)
{
	BOOT(draw_hud) = Y;
	ft_draw_logo(mlx, SMALL_LOGO_PATH, WIN_W / 90 + 10, WIN_H / 60);
	LINE(color) = HUD_DARK_COLOR;
	ft_coor_to_line(0, WIN_H / 7, WIN_W / 9, WIN_H / 7);
	ft_coor_to_line(0, WIN_H / 7 * 2, WIN_W / 9, WIN_H / 7 * 2);
	ft_coor_to_line(0, WIN_H / 7 * 4, WIN_W / 9, WIN_H / 7 * 4);
	ft_coor_to_line(WIN_W / 9, 0, WIN_W / 9, WIN_H - 1);
	ft_coor_to_line(WIN_W - 1, 0, WIN_W - 1, WIN_H - 1);
	ft_coor_to_line(0, 0, 0, WIN_H - 1);
	ft_coor_to_line(0, 0, WIN_W - 1, 0);
	ft_coor_to_line(0, 1, WIN_W - 1, 1);
	ft_coor_to_line(0, WIN_H - 46, WIN_W - 1, WIN_H - 46);
	BOOT(draw_hud) = N;
	return (0);
}

int			ft_draw_destructured_hud(t_mlx *mlx, int step)
{
	BOOT(draw_hud) = Y;
	if (step == 0 && (ft_draw_logo(mlx, SMALL_LOGO_PATH,
					WIN_W / 90 + 10, WIN_H / 60)))
		return (1);
	LINE(color) = HUD_DARK_COLOR;
	if (step == 0)
		ft_coor_to_line(0, WIN_H / 7, WIN_W / 9, WIN_H / 7);
	else if (step == 10)
		ft_coor_to_line(0, WIN_H / 7 * 2, WIN_W / 9, WIN_H / 7 * 2);
	else if (step == 20)
		ft_coor_to_line(WIN_W / 9, 0, WIN_W / 9, WIN_H - 1);
	else if (step == 30)
		ft_coor_to_line(WIN_W - 1, 0, WIN_W - 1, WIN_H - 1);
	else if (step == 40)
		ft_coor_to_line(0, 0, 0, WIN_H - 1);
	else if (step == 50)
		ft_coor_to_line(0, 0, WIN_W - 1, 0);
	else if (step == 60)
		ft_coor_to_line(0, 1, WIN_W - 1, 1);
	else if (step == 70)
		ft_coor_to_line(0, WIN_H - 46, WIN_W - 1, WIN_H - 46);
	BOOT(draw_hud) = N;
	return (0);
}

void		ft_case_check(t_mlx *mlx, int x, int y, int check)
{
	int		i;

	i = 0;
	if (check)
		LINE(color) = HUD_VIBRANT_COLOR;
	else
		LINE(color) = HUD_DARK_COLOR;
	BOOT(draw_hud) = Y;
	ft_coor_to_line(x, y, x + 10, y);
	ft_coor_to_line(x, y, x, y + 10);
	ft_coor_to_line(x, y + 10, x + 10, y + 10);
	ft_coor_to_line(x + 10, y, x + 10, y + 10);
	if (check)
	{
		ft_coor_to_line(x, y, x + 10, y + 10);
		ft_coor_to_line(x, y + 10, x + 10, y);
	}
	else
	{
		LINE(color) = 0;
		while (++i < 10)
			ft_coor_to_line(x + 1, y + i, x + 9, y + i);
	}
	BOOT(draw_hud) = N;
}

void		ft_draw_arrow(t_mlx *mlx, int space, int highlight)
{
	int		i;
	int		x;
	int		y;

	x = LINE(x1);
	y = LINE(y1);
	i = -6;
	space += 6;
	if (MENU(param) == highlight)
		LINE(color) = HUD_VIBRANT_COLOR;
	else
		LINE(color) = HUD_DARK_COLOR;
	BOOT(draw_hud) = Y;
	while (++i < 6)
		ft_coor_to_line(x, y, x + 5, y + i);
	while (--i > -6)
		ft_coor_to_line(x + space, y + i, x + 5 + space, y);
	BOOT(draw_hud) = N;
}

int			ft_draw_logo(t_mlx *mlx, char *path, int x, int y)
{
	int		i;
	int		w;
	char	*line;

	if (!(BOOT(fd) = open(path, O_RDONLY)))
		return (1);
	while (get_next_line(BOOT(fd), &line))
	{
		i = -1;
		w = x - 1;
		while (line[++i] && ++w)
		{
			if (line[i] == 'X')
				IMG(data)[y * WIN_W + w] = LOGO_LIGHT_COLOR;
			else if (line[i] == '.')
				IMG(data)[y * WIN_W + w] = LOGO_VIBRANT_COLOR;
			else if (line[i] == 'o')
				IMG(data)[y * WIN_W + w] = LOGO_DARK_COLOR;
		}
		free(line);
		y++;
	}
	free(line);
	close(BOOT(fd));
	return (0);
}
