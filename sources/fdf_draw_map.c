/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_draw_map.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 18:40:43 by jbard             #+#    #+#             */
/*   Updated: 2018/02/13 21:19:48 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_draw_map_height(t_mlx *mlx, int map)
{
	double	y;
	double	x;

	y = -1;
	while ((MAP(map) % 2 == 0 && ++y < MAP(map_height))
			|| (MAP(map) % 2 == 1 && ++y < MAP(map_width)))
	{
		x = -1;
		while ((MAP(map) % 2 == 0 && ++x + 1 < MAP(map_width))
			|| (MAP(map) % 2 == 1 && ++x + 1 < MAP(map_height)))
		{
			LINE(color) = ft_color(MAP(coor)[map][(int)y][(int)x],
					MAP(coor)[map][(int)y][(int)x + 1], mlx);
			LINE(x1) = ((x + MAP(place_x)) - (y + MAP(place_y))) * MAP(size);
			LINE(y1) = ((x + MAP(place_x)) + (y + MAP(place_y))) / 2 *
				MAP(size) - (double)MAP(coor)[map][(int)y][(int)x]
				* MAP(alt) * MAP(neg);
			LINE(x2) = ((x + MAP(place_x)) - (y + MAP(place_y)) + 1)
				* MAP(size);
			LINE(y2) = ((x + MAP(place_x)) + (y + MAP(place_y)) + 1) / 2 *
				MAP(size) - (double)MAP(coor)[map][(int)y][(int)x + 1] *
				MAP(alt) * MAP(neg);
			ft_draw_line(mlx);
		}
	}
}

static void	ft_draw_map_width(t_mlx *mlx, int map)
{
	double	y;
	double	x;

	x = -1;
	while ((MAP(map) % 2 == 0 && ++x < MAP(map_width))
			|| (MAP(map) % 2 == 1 && ++x < MAP(map_height)))
	{
		y = -1;
		while ((MAP(map) % 2 == 0 && ++y + 1 < MAP(map_height))
			|| (MAP(map) % 2 == 1 && ++y + 1 < MAP(map_width)))
		{
			LINE(color) = ft_color(MAP(coor)[map][(int)y][(int)x],
					MAP(coor)[map][(int)y + 1][(int)x], mlx);
			LINE(x1) = ((x + MAP(place_x)) - (y + MAP(place_y))) * MAP(size);
			LINE(y1) = ((x + MAP(place_x)) + (y + MAP(place_y))) / 2 *
				MAP(size) - (double)MAP(coor)[map][(int)y][(int)x]
				* MAP(alt) * MAP(neg);
			LINE(x2) = ((x + MAP(place_x)) - (y + MAP(place_y)) - 1)
				* MAP(size);
			LINE(y2) = ((x + MAP(place_x)) + (y + MAP(place_y)) + 1) / 2 *
				MAP(size) - (double)MAP(coor)[map][(int)y + 1][(int)x] *
				MAP(alt) * MAP(neg);
			ft_draw_line(mlx);
		}
	}
}

static void	ft_draw_map_diag_1(t_mlx *mlx, int map)
{
	double	y;
	double	x;

	y = 0;
	while ((MAP(map) % 2 == 0 && ++y < MAP(map_height))
			|| (MAP(map) % 2 == 1 && ++y < MAP(map_width)))
	{
		x = -1;
		while ((MAP(map) % 2 == 0 && ++x + 1 < MAP(map_width))
			|| (MAP(map) % 2 == 1 && ++x + 1 < MAP(map_height)))
		{
			LINE(color) = ft_color(MAP(coor)[map][(int)y][(int)x],
					MAP(coor)[map][(int)y - 1][(int)x + 1], mlx);
			LINE(x1) = ((x + MAP(place_x)) - (y + MAP(place_y))) * MAP(size);
			LINE(y1) = ((x + MAP(place_x)) + (y + MAP(place_y))) / 2 *
				MAP(size) - (double)MAP(coor)[map][(int)y][(int)x]
				* MAP(alt) * MAP(neg);
			LINE(x2) = ((x + MAP(place_x)) - (y + MAP(place_y)) + 2)
				* MAP(size);
			LINE(y2) = ((x + MAP(place_x)) + (y + MAP(place_y))) / 2 *
				MAP(size) - (double)MAP(coor)[map][(int)y - 1][(int)x + 1]
				* MAP(alt) * MAP(neg);
			ft_draw_line(mlx);
		}
	}
}

static void	ft_draw_map_diag_2(t_mlx *mlx, int map)
{
	double	y;
	double	x;

	x = -1;
	while ((MAP(map) % 2 == 0 && ++x + 1 < MAP(map_width))
		|| (MAP(map) % 2 == 1 && ++x + 1 < MAP(map_height)))
	{
		y = -1;
		while ((MAP(map) % 2 == 0 && ++y + 1 < MAP(map_height))
			|| (MAP(map) % 2 == 1 && ++y + 1 < MAP(map_width)))
		{
			LINE(color) = ft_color(MAP(coor)[map][(int)y][(int)x],
					MAP(coor)[map][(int)y + 1][(int)x + 1], mlx);
			LINE(x1) = ((x + MAP(place_x)) - (y + MAP(place_y))) * MAP(size);
			LINE(y1) = ((x + MAP(place_x)) + (y + MAP(place_y))) / 2 *
				MAP(size) - (double)MAP(coor)[map][(int)y][(int)x] *
				MAP(alt) * MAP(neg);
			LINE(x2) = ((x + MAP(place_x)) - (y + MAP(place_y))) * MAP(size);
			LINE(y2) = ((x + MAP(place_x)) + (y + MAP(place_y)) + 2) / 2 *
				MAP(size) - (double)MAP(coor)[map][(int)y + 1][(int)x + 1] *
				MAP(alt) * MAP(neg);
			ft_draw_line(mlx);
		}
	}
}

void		ft_draw_map(t_mlx *mlx, int map)
{
	ft_center_map(mlx);
	ft_draw_map_height(mlx, map);
	ft_draw_map_width(mlx, map);
	if (MAP(diag1))
		ft_draw_map_diag_1(mlx, map);
	if (MAP(diag2))
		ft_draw_map_diag_2(mlx, map);
}
