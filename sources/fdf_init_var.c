/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_init_var.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 21:00:10 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 14:41:20 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_mlx		*ft_get_mlx(t_mlx *mlx)
{
	static t_mlx	*save_mlx;

	if (!save_mlx)
		save_mlx = mlx;
	else
		mlx = save_mlx;
	return (mlx);
}

static void	ft_init_max_min(t_mlx *mlx)
{
	int		i;
	int		j;

	i = -1;
	MAP(coor_min) = 0;
	MAP(coor_max) = 0;
	while (++i < MAP(map_height))
	{
		j = -1;
		while (++j < MAP(map_width))
		{
			if (MAP(coor)[0][i][j] < MAP(coor_min))
				MAP(coor_min) = MAP(coor)[0][i][j];
			if (MAP(coor)[0][i][j] > MAP(coor_max))
				MAP(coor_max) = MAP(coor)[0][i][j];
		}
	}
}

void		ft_init_boot_var(t_mlx *mlx)
{
	ft_init_max_min(mlx);
	BOOT(boot_line_y) = 1;
	BOOT(boot) = BOOT_START;
	BOOT(step) = BOOT_STEP_1;
	BOOT(draw_hud) = N;
	ft_get_mlx(mlx);
	ft_init_var(mlx);
}

void		ft_init_var(t_mlx *mlx)
{
	MAP(color1) = MAP_LIGHT_COLOR;
	MAP(color2) = MAP_VIBRANT_COLOR;
	MAP(color3) = MAP_DARK_COLOR;
	MAP(map) = 0;
	MAP(alt) = Y;
	MAP(neg) = 1;
	MAP(flip) = N;
	MAP(diag1) = N;
	MAP(diag2) = N;
	MENU(menu) = 0;
	MENU(param) = 0;
	MENU(color1) = ft_init_color_var(mlx, 1);
	MENU(color2) = ft_init_color_var(mlx, 2);
	MENU(color3) = ft_init_color_var(mlx, 3);
}

void		ft_set_x_y(t_mlx *mlx, int x, int y)
{
	LINE(x1) = x;
	LINE(y1) = y;
}
