/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_init_color.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 20:59:46 by jbard             #+#    #+#             */
/*   Updated: 2018/02/13 20:59:48 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		ft_init_color_var_2(t_mlx *mlx, int c)
{
	if ((c == 1 && MAP(color1) == LIGHT_RED)
			|| (c == 2 && MAP(color2) == VIBRANT_RED)
			|| (c == 3 && MAP(color3) == DARK_RED))
		return (4);
	else if ((c == 1 && MAP(color1) == LIGHT_ORANGE)
			|| (c == 2 && MAP(color2) == VIBRANT_ORANGE)
			|| (c == 3 && MAP(color3) == DARK_ORANGE))
		return (5);
	if ((c == 1 && MAP(color1) == LIGHT_YELLOW)
			|| (c == 2 && MAP(color2) == VIBRANT_YELLOW)
			|| (c == 3 && MAP(color3) == DARK_YELLOW))
		return (6);
	return (7);
}

int		ft_init_color_var(t_mlx *mlx, int c)
{
	if ((c == 1 && MAP(color1) == LIGHT_GREEN)
			|| (c == 2 && MAP(color2) == VIBRANT_GREEN)
			|| (c == 3 && MAP(color3) == DARK_GREEN))
		return (0);
	else if ((c == 1 && MAP(color1) == LIGHT_BLUE)
			|| (c == 2 && MAP(color2) == VIBRANT_BLUE)
			|| (c == 3 && MAP(color3) == DARK_BLUE))
		return (1);
	else if ((c == 1 && MAP(color1) == LIGHT_PURPLE)
			|| (c == 2 && MAP(color2) == VIBRANT_PURPLE)
			|| (c == 3 && MAP(color3) == DARK_PURPLE))
		return (2);
	else if ((c == 1 && MAP(color1) == LIGHT_PINK)
			|| (c == 2 && MAP(color2) == VIBRANT_PINK)
			|| (c == 3 && MAP(color3) == DARK_PINK))
		return (3);
	return (ft_init_color_var_2(mlx, c));
}

int		ft_init_color_light(int color)
{
	if (color == 0)
		return (LIGHT_GREEN);
	else if (color == 1)
		return (LIGHT_BLUE);
	else if (color == 2)
		return (LIGHT_PURPLE);
	else if (color == 3)
		return (LIGHT_PINK);
	else if (color == 4)
		return (LIGHT_RED);
	else if (color == 5)
		return (LIGHT_ORANGE);
	else if (color == 6)
		return (LIGHT_YELLOW);
	else
		return (LIGHT_WHITE);
}

int		ft_init_color_vibrant(int color)
{
	if (color == 0)
		return (VIBRANT_GREEN);
	else if (color == 1)
		return (VIBRANT_BLUE);
	else if (color == 2)
		return (VIBRANT_PURPLE);
	else if (color == 3)
		return (VIBRANT_PINK);
	else if (color == 4)
		return (VIBRANT_RED);
	else if (color == 5)
		return (VIBRANT_ORANGE);
	else if (color == 6)
		return (VIBRANT_YELLOW);
	else
		return (VIBRANT_WHITE);
}

int		ft_init_color_dark(int color)
{
	if (color == 0)
		return (DARK_GREEN);
	else if (color == 1)
		return (DARK_BLUE);
	else if (color == 2)
		return (DARK_PURPLE);
	else if (color == 3)
		return (DARK_PINK);
	else if (color == 4)
		return (DARK_RED);
	else if (color == 5)
		return (DARK_ORANGE);
	else if (color == 6)
		return (DARK_YELLOW);
	else
		return (DARK_WHITE);
}
