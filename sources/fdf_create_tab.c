/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_create_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/22 12:39:34 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 15:44:32 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include <stdio.h>

static int		ft_check_hexa(char *hexa)
{
	int i;

	if (hexa[1] != '0' || hexa[2] != 'x')
		return (0);
	i = 3;
	while (ft_ishexa(hexa[i]))
		i++;
	if (i != 9)
		return (0);
	return (1);
}

static int		ft_check_line(char *line)
{
	int i;

	i = 0;
	while (line[i])
	{
		if (line[i] == '-')
			i++;
		while (ft_isdigit(line[i]))
			i++;
		if (line[i] == ',' && ft_check_hexa(line + i))
			i += 9;
		if (ft_isspace(line[i]))
			i++;
		else if (line[i] != '\0')
		{
			ft_putendl("error: bad character found");
			return (1);
		}
	}
	return (0);
}

static void		ft_fill_line(t_mlx *mlx, char *line)
{
	int		**coor_temp;
	char	**num;
	int		i;

	i = -1;
	MAP(map_height)++;
	if (!(coor_temp = ft_memalloc(sizeof(int*) * MAP(map_height))))
		ft_malloc_error(mlx);
	if (!(coor_temp[MAP(map_height) - 1] =
				ft_memalloc(sizeof(int) * (MAP(map_width)))))
		ft_malloc_error(mlx);
	while (++i < MAP(map_height) - 1)
		coor_temp[i] = MAP(coor)[0][i];
	free(MAP(coor)[0]);
	if (!(num = ft_strsplit(line, ' ')))
		ft_malloc_error(mlx);
	i = -1;
	while (num[++i])
	{
		coor_temp[MAP(map_height) - 1][i] = ft_atoi(num[i]);
		free(num[i]);
	}
	MAP(coor)[0] = coor_temp;
	free(line);
	free(num);
}

static int		ft_line_len(char *line)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (line[j])
	{
		if (ft_isdigit(line[j]))
		{
			i++;
			while (ft_isdigit(line[j]) && line[j])
				j++;
		}
		if (line[j] != '\0')
			j++;
	}
	return (i);
}

int				ft_create_tab(t_mlx *mlx, char *file)
{
	char	*line;
	int		line_len_temp;
	int		fd;

	if ((fd = open(file, O_RDONLY)) <= 0)
	{
		perror("fdf");
		return (1);
	}
	while (get_next_line(fd, &line))
	{
		if (ft_check_line(line))
			return (1);
		if ((line_len_temp = ft_line_len(line)) == 0
			|| (line_len_temp != MAP(map_width) && MAP(map_width) != 0))
		{
			ft_putendl("error: wrong number of coordinates");
			return (1);
		}
		MAP(map_width) = line_len_temp;
		ft_fill_line(mlx, line);
	}
	close(fd);
	free(line);
	return (0);
}
