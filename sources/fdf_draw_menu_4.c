/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_draw_menu_4.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 20:30:21 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 13:30:03 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_draw_rotation_menu_enter(t_mlx *mlx)
{
	if (MENU(param) % 3 == 2)
	{
		MENU(menu) = 0;
		MENU(param) = 0;
		ft_draw_all(mlx);
	}
	else if (MENU(param) % 3 == 1)
	{
		if (!MAP(flip))
			MAP(flip) = FLIP_ON;
		else
			MAP(flip) = FLIP_OFF;
	}
}

void	ft_draw_color_menu_str_4(t_mlx *mlx)
{
	if (MENU(param) % 4 == 0)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 85, 520,
				HUD_VIBRANT_COLOR, "LIGHT COLOR");
	else if (MENU(param) % 4 == 1)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 80, 570,
				HUD_VIBRANT_COLOR, "VIBRANT COLOR");
	else if (MENU(param) % 4 == 2)
		mlx_string_put(mlx->mlx_ptr, mlx->win, 90, 620,
				HUD_VIBRANT_COLOR, "DARK COLOR");
	else
		mlx_string_put(mlx->mlx_ptr, mlx->win, 110, 670,
				HUD_VIBRANT_COLOR, "RETURN");
}
