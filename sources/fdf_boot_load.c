/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf_boot_load.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 17:38:21 by jbard             #+#    #+#             */
/*   Updated: 2018/02/17 12:28:49 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_draw_loadbar(t_mlx *mlx)
{
	ft_draw_logo(mlx, MEDIUM_LOGO_PATH, WIN_W / 2 - 216, WIN_H / 2 - 400);
	LINE(color) = HUD_DARK_COLOR;
	ft_coor_to_line(WIN_W / 2 - 51, WIN_H / 2, WIN_W / 2 + 51, WIN_H / 2);
	ft_coor_to_line(WIN_W / 2 - 51, WIN_H / 2 + 10,
			WIN_W / 2 + 51, WIN_H / 2 + 10);
	ft_coor_to_line(WIN_W / 2 + 51, WIN_H / 2,
			WIN_W / 2 + 51, WIN_H / 2 + 10);
	ft_coor_to_line(WIN_W / 2 - 51, WIN_H / 2,
			WIN_W / 2 - 51, WIN_H / 2 + 10);
}

static void	ft_draw_load_line(t_mlx *mlx, int state)
{
	static int	color;
	int			i;

	i = 0;
	LINE(y2) = WIN_H / 2 + 9;
	if (state % 10 == 0)
		color = HUD_LIGHT_COLOR;
	if (state % 20 == 0)
		color = HUD_DARK_COLOR;
	LINE(color) = color;
	LINE(x1) = state + WIN_W / 2 - 50 + i;
	LINE(x2) = LINE(x1);
	LINE(y1) = WIN_H / 2 + 1;
	ft_draw_line(mlx);
}

int			ft_draw_loading(t_mlx *mlx)
{
	static int state;

	if (!state)
	{
		state = 0;
		LINE(color) = HUD_DARK_COLOR;
		ft_draw_loadbar(mlx);
		LINE(color) = HUD_VIBRANT_COLOR;
	}
	if (state < 100)
	{
		usleep(1000);
		ft_draw_destructured_hud(mlx, state);
		ft_draw_load_line(mlx, state);
	}
	else
		return (0);
	state++;
	return (1);
}
